export {
    connect,
    Link,
    Redirect,
    Route,
    Switch,
    withRouter,
    useHistory,
    useLocation,
    useRouteMatch,
    useParams
} from '@magento/jpwt-ui/lib/drivers';
export { default as resourceUrl } from '@magento/jpwt-ui/lib/util/makeUrl';
export { default as Adapter } from '@magento/jpwt-ui/lib/drivers/adapter';
