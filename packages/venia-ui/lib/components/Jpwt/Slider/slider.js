import React from 'react';
import Slider from 'infinite-react-carousel';
import classes from './slider.css';

const SliderImg = () => {
    return (
        <div className={classes.root}>
            <Slider dots arrows={false}>
                <div>
                    <h3>1</h3>
                    <p>Newsletter API Extension for Magento 2 helps the developer of store simply subscribe an email to default newsletter subscriber by calling an API</p>
                </div>
                <div>
                    <h3>2</h3>
                    <p>Newsletter API Extension for Magento 2 helps the developer of store simply subscribe an email to default newsletter subscriber by calling an API</p>
                </div>
                <div>
                    <h3>3</h3>
                    <p>Newsletter API Extension for Magento 2 helps the developer of store simply subscribe an email to default newsletter subscriber by calling an API</p>
                </div>
                <div>
                    <h3>4</h3>
                    <p>Newsletter API Extension for Magento 2 helps the developer of store simply subscribe an email to default newsletter subscriber by calling an API</p>
                </div>
                <div>
                    <h3>5</h3>
                    <p>Newsletter API Extension for Magento 2 helps the developer of store simply subscribe an email to default newsletter subscriber by calling an API</p>
                </div>
            </Slider>
        </div>
    )
};

export default SliderImg;