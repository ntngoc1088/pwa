import React from 'react';

const Test = props => {
    const getId = () => {
        return props.match.params.id;
    };
    const demo = (name, val) => {
        return `${name} chung toi tesst ${val}`;
    }
    return (
        <div>
            {getId()}
            {demo('abc', 'bcd')}
            <h1>Chung toi test</h1>
        </div>
    );
};
export default Test;