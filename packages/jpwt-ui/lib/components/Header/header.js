import React, { Suspense } from 'react';
import { shape, string } from 'prop-types';

import Logo from '../Logo';
import { Link, resourceUrl, Route } from '@magento/jpwt-drivers';

import CartTrigger from './cartTrigger';
import NavTrigger from './navTrigger';
import SearchTrigger from './searchTrigger';
import OnlineIndicator from './onlineIndicator';
import { useHeader } from '@magento/jpwt-peregrine/lib/talons/Header/useHeader';

import { mergeClasses } from '../../classify';
import defaultClasses from './header.css';
import Image from "../Image";
import LineH from './images/horizontal-line.svg';

const SearchBar = React.lazy(() => import('../SearchBar'));

const Header = props => {
    const {
        handleSearchTriggerClick,
        hasBeenOffline,
        isOnline,
        searchOpen
    } = useHeader();

    const classes = mergeClasses(defaultClasses, props.classes);
    const rootClass = searchOpen ? classes.open : classes.closed;
    const searchBarFallback = (
        <div className={classes.searchFallback}>
            <div className={classes.input}>
                <div className={classes.loader} />
            </div>
        </div>
    );
    const searchBar = searchOpen ? (
        <Suspense fallback={searchBarFallback}>
            <Route
                render={({ history, location }) => (
                    <SearchBar
                        isOpen={searchOpen}
                        history={history}
                        location={location}
                    />
                )}
            />
        </Suspense>
    ) : null;

    return (
        <header className={rootClass}>
            <div className={classes.toolbar}>
                <OnlineIndicator
                    hasBeenOffline={hasBeenOffline}
                    isOnline={isOnline}
                />
                <div className={classes.logo}>
                    <Link to={resourceUrl('/')}>
                        <Logo height={56} width={166} />
                    </Link>
                </div>
                {/*<div className={classes.headerCenter}>
                    <Image alt={'line'} src={LineH} height={'auto'} width={'auto'}/>
                </div>*/}
                <img className={classes.headerCenter} src={LineH} alt={'line'}/>
                <div className={classes.primaryActions}>
                    <NavTrigger />
                </div>
                {/*<div className={classes.secondaryActions}>
                    <SearchTrigger
                        active={searchOpen}
                        onClick={handleSearchTriggerClick}
                    />
                    <CartTrigger />
                </div>*/}
            </div>
            {searchBar}
        </header>
    );
};

Header.propTypes = {
    classes: shape({
        closed: string,
        logo: string,
        open: string,
        primaryActions: string,
        secondaryActions: string,
        toolbar: string
    })
};

export default Header;
