import React from 'react';
import classes from './contact.css';
import CmsBlock from '../CmsBlock';
import CmsPage from '../../RootComponents/CMS/cms'

const Contact = () => {
    const Header = ({value, name}) => {
        return (<label>{value} {name}</label>);
    };
    return (
        <div className={classes.Contact}>
            {Header({value: 'chung', name: 'test'})}
            <p className="title">Contact</p>
            <p>Chung toi test</p>
            <CmsBlock identifiers={'sale-block'} />
            <CmsPage id={2} />
        </div>
    );
};

export default Contact;