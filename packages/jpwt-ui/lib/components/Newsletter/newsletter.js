import React, { Component } from 'react';
import { shape, string } from 'prop-types';
import defaultClasses from './newsletter.css';
import classify from '../../classify';

import Field from '../Field';
import Button from '../Button';
import { Form } from 'informed';
import TextInput from '../TextInput';
import Icon from '../Icon';
import MailIcon from 'react-feather/dist/icons/mail';
import combine from '../../util/combineValidators';
import { validateEmail, isRequired } from '../../util/formValidators';

class Newsletter extends Component {
    static propTypes = {
        classes: shape({
            actions: string,
            error: string,
            lead: string,
            root: string,
            form: string,
            subscribe: string,
            signUp: string,
            signUpTitle: string,
            signUpText: string,
            inputField: string,
            signUpPolicy: string,
            successMessage: string,
            inProgressMessage: string,
            errorMessage: string
        }),
        createAccountError: shape({
            message: string
        }),
        initialValues: shape({
            email: string,
            firstName: string,
            lastName: string
        })
    };

    static defaultProps = {
        initialValues: {}
    };

    get errorMessage() {
        const { createAccountError } = this.props;

        if (createAccountError) {
            const errorIsEmpty = Object.keys(createAccountError).length === 0;
            if (!errorIsEmpty) {
                return 'An error occurred. Please try again.';
            }
        }
        return '';
    }

    get initialValues() {
        const { initialValues } = this.props;
        const { email, firstName, lastName, ...rest } = initialValues;

        return {
            customer: { email, firstname: firstName, lastname: lastName },
            ...rest
        };
    }

    handleFormSubmit = async ({ email }) => {
        let data = {email: email};
        let url = process.env.MAGENTO_BACKEND_URL + 'rest/V1/newsletter/subscribe';
        this.setState({isInProgress: true});
        fetch(url, {
            method: 'POST',
            mode: 'no-cors',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            this.setState({isInProgress: false});
            return res;
        }).catch(
            err =>  {
                this.setState({isInProgress: false, isError: true});
            }
        );
    };

    render() {
        const { errorMessage, initialValues, props } = this;
        const { classes, isError, isInProgress, isSuccess } = props;
        const mail = <Icon src={MailIcon} size={16} />;

        if (isInProgress) {
            return (
                <div className={classes.root}>
                    <p className={classes.inProgressMessage}>
                        Sending your request...
                    </p>
                </div>
            );
        }

        if (isError) {
            return (
                <div className={classes.root}>
                    <p className={classes.errorMessage}>
                        Looks like you have already signed up, Thanks!
                    </p>
                </div>
            );
        }

        if (isSuccess) {
            return (
                <div className={classes.root}>
                    <p className={classes.successMessage}>
                        Thanks for signing up for our newsletter
                    </p>
                    <small className={classes.signUpPolicy}>
                        emails are subject to our
                        <a href="/privacy-policy"> Privacy Policy</a>
                    </small>
                </div>
            );
        }

        return (
            <div className={classes.root}>
                <h2 className={classes.signUpTitle}>Let's keep in touch</h2>
                <p className={classes.signUpText}>
                    <span>
                        Stay up-to-date with our newest products and best deals
                    </span>
                </p>
                <div className={classes.inputField}>
                    <Form
                        className={classes.form}
                        initialValues={initialValues}
                        onSubmit={this.handleFormSubmit}
                    >
                        <Field label="Email" required={true}>
                            <TextInput
                                field="email"
                                autoComplete="email"
                                validate={combine([isRequired, validateEmail])}
                                validateOnBlur
                                placeholder="Email"
                                label="Email"
                                before={mail}
                            />
                        </Field>
                        <div className={classes.error}>{errorMessage}</div>
                        <div className={classes.actions}>
                            <Button type="submit" priority="high">
                                {'Submit'}
                            </Button>
                        </div>
                    </Form>
                </div>
                <small className={classes.signUpPolicy}>
                    emails are subject to our
                    <a href="/privacy-policy"> Privacy Policy</a>
                </small>
            </div>
        );
    }
}

export default classify(defaultClasses)(Newsletter);