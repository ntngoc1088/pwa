import React from 'react';
import { createTestInstance } from '@magento/jpwt-peregrine';

import PriceAdjustments from '../priceAdjustments';

jest.mock('../giftCardSection', () => 'GiftCardSection');

test('it renders Venia price adjustments', () => {
    // Act.
    const instance = createTestInstance(<PriceAdjustments />);

    // Assert.
    expect(instance.toJSON()).toMatchSnapshot();
});
